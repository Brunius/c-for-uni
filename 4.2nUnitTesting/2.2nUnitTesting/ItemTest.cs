﻿using NUnit.Framework;
using System;

namespace nUnitTesting
{
	[TestFixture()]
	public class ItemTest
	{
		[Test()]
		public void TestItemisID()
		{
			//Create item for testing
			Item testItem = new Item(new string[] {"fred", "me"}, "me", "Fred, mighty programmer");

			//Check item responds to BOTH its IDs, not just one
			Assert.AreEqual(true, testItem.AreYou("fred"));
			Assert.AreEqual(true, testItem.AreYou("me"));
		}

		[Test()]
		public void TestItemShortDescription()
		{
			//Create item for testing
			Item testItem = new Item(new string[] {"fred", "me"}, "me", "Fred, mighty programmer");

			//Check item's short description returns "_desc (firstID)"
			Assert.AreEqual("Fred, mighty programmer (me)", testItem.ShortDescription);
		}

		[Test()]
		public void TestItemFullDescription()
		{
			//Create item for testing
			Item testItem = new Item(new string[] { "fred", "me" }, "me", "Fred, mighty programmer");

			//Check item's full description returns "_desc"
			Assert.AreEqual("Fred, mighty programmer", testItem.FullDescription);
		}
	}
}