﻿using System;
using System.Collections.Generic;

namespace nUnitTesting
{
	public class IdentifiableObject
	{
		List<string> _identifiers = new List<string>();

		protected IdentifiableObject()
		{
		}

		public IdentifiableObject(string[] idents)
		{
			foreach (string s in idents) {
				AddIdentifier(s);
			}
		}

		public bool AreYou(string id)
		{
			foreach (string query in _identifiers) {
				if (id.Equals(query, StringComparison.OrdinalIgnoreCase)) {
					return true;
				}
			}
			return false;
		}

		public string FirstId
		{
			get {
				if (_identifiers[0] != null) {
					return _identifiers[0];
				} else {
					return "";
				}
			}
		}

		public void AddIdentifier(string id)
		{
			_identifiers.Add(id);
		}
	}
}

