﻿using System;
using System.Collections.Generic;

namespace nUnitTesting
{
	public class Inventory
	{
		List<Item> _items = new List<Item>();

		public Inventory()
		{
		}

		public bool HasItem(string id)
		{
			//Cycles through each item, checking if it is id
			//If it is, returns true
			//If every item is checked and is not, returns false
			foreach (Item I in _items) {
				if (I.AreYou(id)) {
					return true;
				}
			}
			return false;
		}

		public void Put(Item itm)
		{
			_items.Add(itm);
		}

		public Item Take(string id)
		{
			//As Fetch, but removes item from inventory afterwards
			//It feels kinda funny to be calling Fetch before it's defined...
			Item temp = this.Fetch(id);
			if (temp != null) {
				_items.Remove(temp);
				return temp;
			} else {
				return null;
			}
		}

		public Item Fetch(string id)
		{
			//Cycles through each item, checking for id match
			//If a match is found, return item
			//If no match is found, return null
			foreach (Item I in _items) {
				if (I.AreYou(id)) {
					return I;
				}
			}
			return null;
		}

		public string ItemList {
			get {
				string returnString = "";
				foreach (Item I in _items) {
					returnString = returnString + "\t" + I.ShortDescription + "\n";
				}
				return returnString;
			}
		}
	}
}

