﻿using NUnit.Framework;
using System;

namespace nUnitTesting
{
	[TestFixture()]
	public class PlayerTest
	{
		[Test()]
		public void PlayerIDTest()
		{
			Player testPlayer = new Player("ItsComrade", "ItsComrade, destroyer of worlds");

			Assert.AreEqual(true, testPlayer.AreYou("me"));
			Assert.AreEqual(true, testPlayer.AreYou("inventory"));
		}

		[Test()]
		public void PlayerLocateItemsTest()
		{
			Player testPlayer = new Player("ItsComrade", "ItsComrade, destroyer of worlds");
			Item M27IAR = new Item(new string[]{ "M27", "IAR", "AR", "gun" }, "M27 IAR", "The USMC standard issue Automatic Rifle. Takes 5.56 NATO");
			Item Ammo556 = new Item(new string[]{ "5.56", "ammo", "IAR ammo", "rifle ammo" }, "ammo", "NATO-standard 5.56x45 ammunition in a 30 round magazine");
			Item JakeBook = new Item(new string[]{ "book", "jake book", "leather book" }, "Jake the Cat", "A leather-bound tome describing a deity called 'Jake the Cat'");

			testPlayer.Inventory.Put(M27IAR);
			testPlayer.Inventory.Put(Ammo556);
			testPlayer.Inventory.Put(JakeBook);

			Assert.AreEqual(M27IAR, testPlayer.Locate("M27"));

			//Bonus: Player can locate themselves?
			Assert.AreEqual(testPlayer, testPlayer.Locate("me"));

			//EXTRA Bonus: Player locates nothing?
			Assert.AreEqual(null, testPlayer.Locate("M249"));
		}

		[Test()]
		public void PlayerFullDescriptionTest()
		{
			Player testPlayer = new Player("ItsComrade", "ItsComrade, destroyer of worlds");
			Item M27IAR = new Item(new string[]{ "M27 IAR", "M27", "IAR", "AR", "gun" }, "M27 IAR", "M27 IAR - The USMC standard issue automatic rifle. Takes 5.56 NATO");
			Item Ammo556 = new Item(new string[]{ "5.56 NATO", "5.56", "ammo", "IAR ammo", "rifle ammo" }, "5.56 NATO", "NATO-standard 5.56x45 ammunition in a 30 round magazine");
			Item JakeBook = new Item(new string[]{ "Jake the Cat", "book", "jake book", "leather book" }, "Jake the Cat", "A leather-bound tome describing a deity called 'Jake the Cat'");

			testPlayer.Inventory.Put(M27IAR);
			testPlayer.Inventory.Put(Ammo556);
			testPlayer.Inventory.Put(JakeBook);

			Assert.AreEqual("ItsComrade, destroyer of worlds (ItsComrade)\nYou are carrying:\n\tM27 IAR - The USMC standard issue automatic rifle. Takes 5.56 NATO (M27 IAR)\n\tNATO-standard 5.56x45 ammunition in a 30 round magazine (5.56 NATO)\n\tA leather-bound tome describing a deity called 'Jake the Cat' (Jake the Cat)\n", testPlayer.FullDescription);
		}
	}
}

