﻿using System;

namespace CounterTest
{
	class MainClass
	{
		public static void PrintCounters(Counter[] counters)
		{
			//for each whatever
			foreach (Counter c in counters) {
				Console.WriteLine ("{0} in {1}", c.Name, c.Value);
			}
		}

		public static void Main (string[] args)
		{
			Counter[] myCounters = new Counter[3];

			myCounters[0] = new Counter("Counter 1");
			myCounters[1] = new Counter("Counter 2");
			myCounters[2] = myCounters[0];

			for (int i = 0; i < 4; i++) {
				myCounters[0].increment();
			}

			for (int i = 0; i < 9; i++) {
				myCounters[1].increment();
			}

			PrintCounters(myCounters);

			myCounters[2].reset();

			PrintCounters(myCounters);
			Console.ReadLine ();
		}
	}
}
