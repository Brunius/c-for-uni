﻿using System;
using System.Collections.Generic;

using SwinGameSDK;

namespace MyGame
{
	public class Drawing
	{
		private readonly List<Shape> _shapes;

		private Color _background;

		public Drawing (Color background)
		{
			_shapes = new List<Shape> ();
			_background = background;
		}

		public Drawing () : this (Color.White)
		{

		}

		public Color Background
		{
			get {
				return _background;
			}

			set {
				_background = value;
			}
		}

		public int ShapeCount
		{
			get
			{
				return _shapes.Count;
			}
		}

		public void AddShape (Shape addMe)
		{
			_shapes.Add (addMe);
		}

		public void Draw ()
		{
			SwinGame.ClearScreen (_background);
			foreach (Shape s in _shapes)
			{
				s.Draw();
			}
		}

		public void SelectShapesAt(Point2D pt)
		{
			foreach (Shape s in _shapes)
			{
				if (s.IsAt (pt))
				{
					s.Selected = true;
				}
				else
				{
					s.Selected = false;
				}
			}
		}

		public List<Shape> SelectedShapes
		{
			get {
				List<Shape> result;
				result = new List<Shape>();
				foreach(Shape s in _shapes) {
					if (s.Selected) {
						result.Add(s);
					}
				}
				return result;
			}
		}

		public void DeleteSelected (){
			foreach (Shape s in SelectedShapes)
			{
				_shapes.Remove (s);
			}
		}
	}
}