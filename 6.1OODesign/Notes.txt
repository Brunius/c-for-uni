SpellParent (abstract?)
	has name (String) (+ Getter)
	has type (enum, teleport, heal, etc.)
	has fail method (abstract)
	has difficulty (enum, low, medium, high)
	has use method (abstract), (pass caster by ref)
	has school (enum, teleport, heal, etc.)

SpellTeleportation
	override school
	override fail to description
	override use to description

SpellHealing
	override school
	override fail to description
	override use to description

SpellTransmogrification
	override school
	override fail to description
	override use to description

Student
	has skill (list, Tuple of school/difficulty)
	has name (String) (+Getter)
	has spell book (list of spells)
	has injured (int 0-10)
	has cast method (pass spell name)
	has add method (pass spell)

Fail chance is defined by skill * difficulty.
Difficulty is low/medium/high and failure associated is 0.1/0.3/0.5
Skill is low/medium/high per school, with associated multipliers being 1.5/1/0.5

So a low skill caster attempting a high skilled spell has a failure chance of 0.5*1.5 = 0.75, or 75%.
A low skill caster attempting a low skilled spell has a failure chance of 1.5*0.1 = 0.15, or 15%
A high skill caster attempting a low skilled spell has a failure chance of 0.5*0.1 = 0.05, or 5%

Failure may have unexpected consequences. These are handled during the Cast method, by the Spell's Fail method. The Fail method has direct access to the casting Student's object, and can change things there. These may be injuries, or simply a description of a change (eg. "The (Polymorph) goes horribly wrong, and (Jim) is trapped in the form of a frog forever.")



When a student is asked to cast a spell, they ask each spell in their spellbook what its name is. If the name matches, they cast that spell, and processing is handed off to the spell. If no spell matches the given name, nothing happens.

When a spell's 'Use' method is invoked, it passes the caster through to the 'failCheck' method in the same object, which then asks the caster what their skill level with that school is. Given various skill levels and difficulty levels, it then calculates the failure chance, and checks whether the spell casts successfully or fails. This is passed back to the 'Use' method as a boolean, and the success or failure dealt with as necessary.