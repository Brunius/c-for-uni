﻿using System;

namespace nUnitTesting
{
	public class Bag :Item
	{
		
		Inventory _inventory = new Inventory();

		public Bag(string[] ids, string name, string desc)
			: base(ids, name, desc){}

		public GameObject Locate(string id){
			if (id == base.Name) {
				return this;
			} else {
				return _inventory.Fetch(id);
			}
		}

		public override string FullDescription {
			get{
				return "In the " + base.Name + " you can see:\n" + _inventory.ItemList;
			}
		}

		public Inventory Inventory {
			get {
				return _inventory;
			}
		}
	}
}

