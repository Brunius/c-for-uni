﻿using NUnit.Framework;
using System;

namespace nUnitTesting
{
	[TestFixture()]
	public class InventoryTest
	{
		[Test()]
		public void InventoryItemContainerTest()
		{
			//This test will check whether the inventory correctly
			//contains items.

			//Create two items to be inserted to the inventory
			Item sword = new Item(new string[]{"sword", "bronze sword", "sw"}, "sword", "a bronze sword");
			Item axe = new Item(new string[]{ "axe", "pig iron axe", "ax" }, "axe", "a pig iron axe");

			//Create inventory
			Inventory pi = new Inventory();
			pi.Put(sword);

			//First test - does it have the sword in it? (It should)
			Assert.AreEqual(true, pi.HasItem("sw"));

			//Second test - does it have the axe in it? (It shouldn't)
			Assert.AreEqual(false, pi.HasItem("ax"));

			//Now, put the axe in it. It should now have both items in it
			pi.Put(axe);

			//Third test - does it have both in it?
			Assert.AreEqual(true, pi.HasItem("sw"));
			Assert.AreEqual(true, pi.HasItem("ax"));
		}

		[Test()]
		public void InventoryFetchTest()
		{
			//This test will check whether fetch returns an item (leaving it in the inventory)

			//Setup
			Item sword = new Item(new string[]{"sword", "bronze sword", "sw"}, "sword", "a bronze sword");
			Item axe = new Item(new string[]{ "axe", "pig iron axe", "ax" }, "axe", "a pig iron axe");
			Inventory pi = new Inventory();
			pi.Put(sword);
			pi.Put(axe);
			//Setup complete

			//First test - does searching for the item find it?
			Assert.AreEqual(sword, pi.Fetch("sw"));

			//Second test - does the inventory still have the item?
			Assert.AreEqual(true, pi.HasItem("sw"));
		}

		[Test()]
		public void InventoryTakeTest()
		{
			//This test will check whether take returns an item (removing it from the inventory)-

			//Setup
			Item sword = new Item(new string[]{"sword", "bronze sword", "sw"}, "sword", "a bronze sword");
			Item axe = new Item(new string[]{ "axe", "pig iron axe", "ax" }, "axe", "a pig iron axe");
			Inventory pi = new Inventory();
			pi.Put(sword);
			pi.Put(axe);
			//Setup complete

			//First test - does taking the item return the correct item?
			Assert.AreEqual(sword, pi.Take("sw"));

			//Second test - does the inventory still have the item?
			Assert.AreEqual(false, pi.HasItem("sw"));
		}

		[Test()]
		public void InventoryItemListTest()
		{
			//Setup
			Item sword = new Item(new string[]{"sword", "bronze sword", "sw"}, "sword", "a bronze sword");
			Item axe = new Item(new string[]{ "axe", "pig iron axe", "ax" }, "axe", "a pig iron axe");
			Inventory pi = new Inventory();
			pi.Put(sword);
			pi.Put(axe);
			//Setup complete

			//First test - does it output a list of items?
			Assert.AreEqual("\ta bronze sword (sword)\n\ta pig iron axe (axe)\n", pi.ItemList);
		}
	}
}

