﻿using NUnit.Framework;
using System;

namespace nUnitTesting
{
	[TestFixture()]
	public class IdentifiableObjectTest
	{
		[Test()]
		public void AreYouTest()
		{
			IdentifiableObject id = new IdentifiableObject(new string[] { "fred", "bob" });

			//Test Are You
			Assert.AreEqual(true, id.AreYou("fred"));

			//Test Not Are You
			Assert.AreEqual(false, id.AreYou("charlie"));

			//Test Case Sensitive
			Assert.AreEqual(true, id.AreYou("FRED"));
		}

		[Test()]
		public void FirstIDTest()
		{
			IdentifiableObject id = new IdentifiableObject(new string[] { "fred", "bob" });

			//Test First ID
			Assert.AreEqual("fred", id.FirstId);
		}

		[Test()]
		public void AddIDTest()
		{
			IdentifiableObject id = new IdentifiableObject(new string[] { "fred", "bob" });

			//Test Add ID
			id.AddIdentifier("wilma");
			Assert.AreEqual(true, id.AreYou("wilma"));
		}
	}
}

