﻿using System;

namespace nUnitTesting
{
	public abstract class Command
	{
		public Command(string[] ids)
		{
		}

		public abstract string Execute(Player p, string[] text)
		{
		}
	}
}

