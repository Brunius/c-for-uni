using NUnit.Framework;
using System;

namespace nUnitTesting
{
	[TestFixture()]
	public class BagTest
	{
		[Test()]
		public void LocatesItemTest()
		{
			//"You can add items to the Bag, and locate the items in its inventory, this
			//returns items the bag has and the item remains in the bag's inventory"

			//Create two items to be inserted to the inventory
			Item sword = new Item(new string[]{"sword", "bronze sword", "sw"}, "sword", "a bronze sword");
			Item axe = new Item(new string[]{ "axe", "pig iron axe", "ax" }, "axe", "a pig iron axe");

			//Create bag to have items inserted
			Bag leatherBag = new Bag(new string[]{"leather bag", "bag"}, "bag", "A worn leather bag");

			leatherBag.Inventory.Put(sword);
			leatherBag.Inventory.Put(axe);

			Assert.AreEqual(sword, leatherBag.Locate("sword"));

			//Bonus: bag can locate itself
			//"The bag returns itself is asked to locate one of its identifiers"
			Assert.AreEqual(leatherBag, leatherBag.Locate("bag"));

			//Bonus bonus: bag returns null object if asked to locate object it does not have
			//"The bag returns a null/nil object if asked to locate something it does not have"
			Assert.AreEqual(null, leatherBag.Locate("pig"));
		}

		[Test()]
		public void BagDescriptionTest()
		{
			//Create two items to be inserted to the inventory
			Item sword = new Item(new string[]{"sword", "bronze sword", "sw"}, "sword", "a bronze sword");
			Item axe = new Item(new string[]{ "axe", "pig iron axe", "ax" }, "axe", "a pig iron axe");

			//Create bag to have items inserted
			Bag leatherBag = new Bag(new string[]{"leather bag", "bag"}, "bag", "A worn leather bag");

			leatherBag.Inventory.Put(sword);
			leatherBag.Inventory.Put(axe);

			Assert.AreEqual("In the bag you can see:\n\ta bronze sword (sword)\n\ta pig iron axe (axe)\n", leatherBag.FullDescription);
		}

		[Test()]
		public void BagInceptionTest()
		{
			//Create two bag objects. Place b2 in b1s inventory. Test that b1 can locate b2. Test that b2 can locate other items in b1. Test that b1 CANNOT locate items in b2.
			Item b11 = new Item(new string[]{"b11", "b1one"}, "b11", "B1 test object 1");
			Item b12 = new Item(new string[]{"b12", "b1two"}, "b11", "B1 test object 2");

			Bag b1 = new Bag(new string[]{ "b1", "bone" }, "b1", "Bag 1");
			b1.Inventory.Put(b11);
			b1.Inventory.Put(b12);

			Item b21 = new Item(new string[]{"b21", "b2one"}, "b21", "B2 test object 1");
			Item b22 = new Item(new string[]{"b22", "b2two"}, "b21", "B2 test object 2");

			Bag b2 = new Bag(new string[]{ "b2", "btwo" }, "b2", "Bag 2");
			b2.Inventory.Put(b21);
			b2.Inventory.Put(b22);

			b1.Inventory.Put(b2);

			//Can b1 locate b2?
			Assert.AreEqual(b2, b1.Locate("b2"));

			//Can b1 locate b11 and b12?
			Assert.AreEqual(b11, b1.Locate("b11"));
			Assert.AreEqual(b12, b1.Locate("b12"));

			//Can b1 locate b21 and b22?
			Assert.AreEqual(null, b1.Locate("b21"));
		}
	}
}

