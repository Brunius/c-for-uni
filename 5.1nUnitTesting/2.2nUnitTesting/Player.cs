﻿using System;

namespace nUnitTesting
{
	public class Player : GameObject
	{
		Inventory _inventory = new Inventory();

		public Player(string name, string desc) : base (new string[] {"me", "inventory"}, name, desc)
		{
			
		}

		public GameObject Locate(string id)
		{
			if (this.AreYou(id)) {
				return this;
			} else {
				return _inventory.Fetch(id);
				//Note that this will return a null pointer
				//	IF the item does not exist.
			}
		}

		public override string FullDescription
		{
			get {
				return base.ShortDescription + "\nYou are carrying:\n" + _inventory.ItemList;
			}
		}

		public Inventory Inventory {
			get {
				return _inventory;
			}
		}
	}
}

