﻿using System;

namespace nUnitTesting
{
	public class GameObject : IdentifiableObject
	{
		private string _description;

		private string _name;

		public GameObject(string[] ids, string name, string desc)
		{
			foreach (string s in ids) {
				base.AddIdentifier(s);
			}

			_name = name;

			_description = desc;
		}

		public string Name {
			get {
				return _name;
			}
		}

		public string ShortDescription {
			get {
				return _description + " (" + _name + ")";
			}
		}

		public virtual string FullDescription {
			get {
				return _description;
			}
		}
	}
}

