#include "Counter.h"

class Clock {
	Counter _secnd = Counter("Seconds");
	Counter _minut = Counter("Minutes");
	Counter _hours = Counter("Hours");
public:
	Clock();
	void Tick();
	void Reset();
	std::string Time();
};

Clock::Clock() {
};

//Time getter
std::string Clock::Time() {
	char tempArray[9] = "";
	sprintf(tempArray, "%d:%d:%d", _hours.Value(), _minut.Value(), _secnd.Value());
	return tempArray;
}

//Interaction functions

void Clock::Tick() {
	_secnd.Increment();
	if (_secnd.Value() == 60) {
		_secnd.Reset();
		_minut.Increment();
		if (_minut.Value() == 60) {
			_minut.Reset();
			_hours.Increment();
			if (_hours.Value() == 24) {
				_hours.Reset();
			}
		}
	}
}

void Clock::Reset() {
	_secnd.Reset();
	_minut.Reset();
	_hours.Reset();
}

#include <string>

class Counter {
	int _count;
	std::string _name;
public:
	Counter(std::string name);
	void Increment();
	void Reset();
	std::string Name();
	int Value();
};

Counter::Counter(std::string name) {
	_name = name;
	_count = 0;
}

//Getters

std::string Counter::Name() {
	return _name;
}

int Counter::Value() {
	return _count;
}

//Interaction functions

void Counter::Increment() {
	_count++;
}

void Counter::Reset() {
	_count = 0;
}

// Clock.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
//#include "Counter.h"
#include "Clock.h"
#include <iostream>

int main()
{
	Counter countMe = Counter("Test Counter");
	printf("Number at initilaisation: %d\n", countMe.Value());
	for (int i = 20; i > 0; i--) {
		countMe.Increment();
	}
	printf("Number after 20 Increments: %d\n", countMe.Value());
	countMe.Reset();
	printf("Number after reset: %d\n", countMe.Value());
	std::cin.ignore();
	Clock noCountMe = Clock();
	printf("Time at initialisation: %s\n", noCountMe.Time().c_str());
	for (int i = 70; i > 0; i--) {
		noCountMe.Tick();
	}
	printf("Time after 70 ticks: %s\n", noCountMe.Time().c_str());
	noCountMe.Reset();
	printf("Time after reset: %s\n", noCountMe.Time().c_str());
	std::cin.ignore();
	return 0;
}

