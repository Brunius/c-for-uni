#include <string>

class Counter {
	int _count;
	std::string _name;
public:
	Counter(std::string name);
	void Increment();
	void Reset();
	std::string Name();
	int Value();
};

Counter::Counter(std::string name) {
	_name = name;
	_count = 0;
}

//Getters

std::string Counter::Name() {
	return _name;
}

int Counter::Value() {
	return _count;
}

//Interaction functions

void Counter::Increment() {
	_count++;
}

void Counter::Reset() {
	_count = 0;
}