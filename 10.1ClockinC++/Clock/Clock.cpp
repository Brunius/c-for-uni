// Clock.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
//#include "Counter.h"
#include "Clock.h"
#include <iostream>

int main()
{
	Counter countMe = Counter("Test Counter");
	printf("Number at initilaisation: %d\n", countMe.Value());
	for (int i = 20; i > 0; i--) {
		countMe.Increment();
	}
	printf("Number after 20 Increments: %d\n", countMe.Value());
	countMe.Reset();
	printf("Number after reset: %d\n", countMe.Value());
	std::cin.ignore();
	Clock noCountMe = Clock();
	printf("Time at initialisation: %s\n", noCountMe.Time().c_str());
	for (int i = 70; i > 0; i--) {
		noCountMe.Tick();
	}
	printf("Time after 70 ticks: %s\n", noCountMe.Time().c_str());
	noCountMe.Reset();
	printf("Time after reset: %s\n", noCountMe.Time().c_str());
	std::cin.ignore();
	return 0;
}

