#include "Counter.h"

class Clock {
	Counter _secnd = Counter("Seconds");
	Counter _minut = Counter("Minutes");
	Counter _hours = Counter("Hours");
public:
	Clock();
	void Tick();
	void Reset();
	std::string Time();
};

Clock::Clock() {
};

//Time getter
std::string Clock::Time() {
	char tempArray[9] = "";
	sprintf(tempArray, "%d:%d:%d", _hours.Value(), _minut.Value(), _secnd.Value());
	return tempArray;
}

//Interaction functions

void Clock::Tick() {
	_secnd.Increment();
	if (_secnd.Value() == 60) {
		_secnd.Reset();
		_minut.Increment();
		if (_minut.Value() == 60) {
			_minut.Reset();
			_hours.Increment();
			if (_hours.Value() == 24) {
				_hours.Reset();
			}
		}
	}
}

void Clock::Reset() {
	_secnd.Reset();
	_minut.Reset();
	_hours.Reset();
}