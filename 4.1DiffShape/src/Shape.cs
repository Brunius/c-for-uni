﻿using System;

using SwinGameSDK;

namespace MyGame
{
	public abstract class Shape
	{
		private Color _color;
		private float _x, _y;
		private Boolean _selected;

		public Shape(Color clr)
		{
			_color = clr;
		}

		public Shape (): this (Color.Yellow)
		{
		}

		public Color Color
		{
			get {
				return _color;
			}

			set {
				_color = value;
			}
		}

		public virtual float X
		{
			get {
				return _x;
			}

			set {
				_x = value;
			}
		}

		public virtual float Y
		{
			get {
				return _y;
			}

			set {
				_y = value;
			}
		}

		public Boolean Selected
		{
			get
			{
				return _selected;
			}
			set
			{
				_selected = value;
			}
		}

		public abstract void Draw ();

		public abstract void DrawOutline ();

		public abstract Boolean IsAt (Point2D pt);
	}
}

