using System;
using SwinGameSDK;

namespace MyGame
{
    public class GameMain
    {

		private enum ShapeKind
		{
			Rectangle,
			Circle,
			Line
		}

        public static void Main()
        {
			//-----------------------------------------------
			Drawing drawing;
			drawing = new Drawing();
			ShapeKind kindToAdd = new ShapeKind();
			kindToAdd = ShapeKind.Circle;
			//-----------------------------------------------

            //Open the game window
            SwinGame.OpenGraphicsWindow("GameMain", 800, 600);
            //SwinGame.ShowSwinGameSplashScreen();
            
            //Run the game loop
            while(false == SwinGame.WindowCloseRequested())
            {
                //Fetch the next batch of UI interaction
                SwinGame.ProcessEvents();
                
                //Clear the screen and draw the framerate
                SwinGame.ClearScreen(Color.White);
                SwinGame.DrawFramerate(0,0);

				//-------------------------------------------
				if (SwinGame.KeyTyped (KeyCode.RKey))
				{
					kindToAdd = ShapeKind.Rectangle;
				}

				if (SwinGame.KeyTyped (KeyCode.CKey))
				{
					kindToAdd = ShapeKind.Circle;
				}

				if (SwinGame.KeyTyped (KeyCode.LKey))
				{
					kindToAdd = ShapeKind.Line;
				}

				if(SwinGame.MouseClicked(MouseButton.LeftButton))
				{
					Shape newShape;

			/*		if (kindToAdd == ShapeKind.Circle)
					{
						newShape = new Circle ();
					}
					else if (kindToAdd == ShapeKind.Rectangle)
					{
						newShape = new Rectangle ();
					}
					else
					{
						newShape = new Line();
					}
*/
					switch (kindToAdd)
					{
					case ShapeKind.Circle:
						newShape = new Circle();
						break;
					case ShapeKind.Rectangle:
						newShape = new Rectangle ();
						break;
					case ShapeKind.Line:
						newShape = new Line();
						break;
					default:
						Console.WriteLine ("You broke something");
						newShape = new Circle ();
						break;
					}

					newShape.X = SwinGame.MouseX ();
					newShape.Y = SwinGame.MouseY ();

					drawing.AddShape (newShape);
				}

				if (SwinGame.MouseClicked (MouseButton.RightButton))
				{
					drawing.SelectShapesAt (SwinGame.MousePosition());
				}

				if (SwinGame.KeyTyped (KeyCode.SpaceKey))
				{
					drawing.Background = SwinGame.RandomRGBColor (0xFF);
				}

				if (SwinGame.KeyTyped (KeyCode.DeleteKey) || SwinGame.KeyTyped(KeyCode.BackspaceKey))
				{
					drawing.DeleteSelected ();
				}

				drawing.Draw ();
				//-------------------------------------------
                
                //Draw onto the screen
                SwinGame.RefreshScreen(60);
            }
        }
    }
}