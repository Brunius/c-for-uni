﻿using System;

using SwinGameSDK;

namespace MyGame
{
	public class Line : Shape
	{
		private Point2D _firstPt, _secondPt;
		private Color _color;

		public Line (Color clr, Point2D pt1, Point2D pt2)
		{
			_color = clr;
			_firstPt = pt1;
			_secondPt = pt2;
		}

		public Line (Color clr, float X1, float Y1, float X2, float Y2)
		{
			_color = clr;
			_firstPt.X = X1;
			_firstPt.Y = Y1;
			_secondPt.X = X2;
			_secondPt.Y = Y2;
		}

		public Line (Color clr, float X, float Y) : this (clr, X - 50, Y - 50, X + 50, Y + 50){}

		public Line () : this(Color.Red, 0, 0){}

		public override float X
		{
			get
			{
				return (_firstPt.X + _secondPt.X) / 2;
			}
			set
			{
				float Xtemp = (_firstPt.X - X) + value;
				_secondPt.X = (_secondPt.X - X) + value;
				_firstPt.X = Xtemp;
			}
		}

		public override float Y
		{
			get
			{
				return (_firstPt.Y + _secondPt.Y) / 2;
			}
			set
			{
				float Ytemp = (_firstPt.Y - Y) + value;
				_secondPt.Y = (_secondPt.Y - Y) + value;
				_firstPt.Y = Ytemp;
			}
		}

		public Point2D PointOne
		{
			get
			{
				return _firstPt;
			}
			set
			{
				_firstPt = value;
			}
		}

		public Point2D PointTwo
		{
			get
			{
				return _secondPt;
			}
			set
			{
				_secondPt = value;
			}
		}

		public override void Draw()
		{
			if (Selected)
			{
				DrawOutline ();
			}

			SwinGame.DrawLine (_color, _firstPt, _secondPt);
		}

		public override void DrawOutline() {
			SwinGame.FillCircle (Color.Black, _firstPt, 2);
			SwinGame.FillCircle (Color.Black, _secondPt, 2);
		}

		public override bool IsAt (Point2D pt)
		{
			return SwinGame.PointOnLine (pt, _firstPt.X, _firstPt.Y, _secondPt.X, _secondPt.Y);
		}
	}
}

