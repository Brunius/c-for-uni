﻿using NUnit.Framework;
using System;

using System.IO;

namespace OODesign
{
	[TestFixture()]
	public class StudentTest
	{
		[Test()]
		public void StudentSetupTest()
		{
			Student testStudent = new Student("Jim", difficulty.Low, difficulty.Low, difficulty.Low);

			//Test name is correct.
			Assert.AreEqual("Jim", testStudent.Name);

			//Test skills are as expected
			Assert.AreEqual(difficulty.Low, testStudent.Skill(type.Transmogrification));
			Assert.AreEqual(difficulty.Low, testStudent.Skill(type.Healing));
			Assert.AreEqual(difficulty.Low, testStudent.Skill(type.Teleportation));
		}
		[Test()]
		public void SpellHealingTest()
		{
			SpellHealing healingSpell = new SpellHealing("Cure Light Wounds", difficulty.Low, "heals 1d8 damage");
			Student testStudent = new Student("Jim", difficulty.Low, difficulty.Low, difficulty.Low);

			//Test name is correct
			Assert.AreEqual("Cure Light Wounds", healingSpell.Name);

			testStudent.Add(healingSpell);
			//This is really cludgy but I can't figure out how to make nunit accept multiple options for 'actual' ¯\_(ツ)_/¯
			//It's pretty unlikely that it would throw either a pass or fail 100 times in a row,
			//but if I were testing for production I'd probably increase the number (or decouple the objects properly...)
			for (int i = 100; i > 0; i--) {
				//-------------------------------------------------
				StringWriter sw = new StringWriter();
				Console.SetOut(sw);
				//-------------------------------------------------

				testStudent.Cast("Cure Light Wounds");
				string expectedPass = testStudent.Name + " heals 1d8 damage\r\n";
				string expectedFail = testStudent.Name + " is hurt as " + healingSpell.Name + " fails!\r\n";
				if (sw.ToString() == expectedFail) {
					Assert.AreEqual(expectedFail, sw.ToString());
				} else {
					Assert.AreEqual(expectedPass, sw.ToString());
				}
				sw.Dispose();
			}
		}

		[Test()]
		public void SpellTransmogTest()
		{
			SpellTransmogrification spellTransmog = new SpellTransmogrification("Giant's Strength", difficulty.Low, "doubles in size");
			Student testStudent = new Student("Jim", difficulty.Low, difficulty.Low, difficulty.Low);

			//Test name is correct
			Assert.AreEqual("Giant's Strength", spellTransmog.Name);

			testStudent.Add(spellTransmog);
			//This is really cludgy but I can't figure out how to make nunit accept multiple options for 'actual' ¯\_(ツ)_/¯
			//It's pretty unlikely that it would throw either a pass or fail 100 times in a row,
			//but if I were testing for production I'd probably increase the number (or decouple the objects properly...)
			for (int i = 100; i > 0; i--) {
				//-------------------------------------------------
				StringWriter sw = new StringWriter();
				Console.SetOut(sw);
				//-------------------------------------------------

				testStudent.Cast("Giant's Strength");
				string expectedPass = testStudent.Name + " doubles in size\r\n";
				string expectedFail = testStudent.Name + " is grown to enormous size as " + spellTransmog.Name + " fails!\r\n";
				if (sw.ToString() == expectedFail) {
					Assert.AreEqual(expectedFail, sw.ToString());
				} else {
					Assert.AreEqual(expectedPass, sw.ToString());
				}
				sw.Dispose();
			}
		}

		[Test()]
		public void SpellTeleTest()
		{
			SpellTeleportation spellTele = new SpellTeleportation("Dimensional Door", difficulty.Low, "crosses the room with a single step");
			Student testStudent = new Student("Jim", difficulty.Low, difficulty.Low, difficulty.Low);

			//Test name is correct
			Assert.AreEqual("Dimensional Door", spellTele.Name);

			testStudent.Add(spellTele);
			//This is really cludgy but I can't figure out how to make nunit accept multiple options for 'actual' ¯\_(ツ)_/¯
			//It's pretty unlikely that it would throw either a pass or fail 100 times in a row,
			//but if I were testing for production I'd probably increase the number (or decouple the objects properly...)
			for (int i = 100; i > 0; i--) {
				//-------------------------------------------------
				StringWriter sw = new StringWriter();
				Console.SetOut(sw);
				//-------------------------------------------------

				testStudent.Cast("Dimensional Door");
				string expectedPass = testStudent.Name + " crosses the room with a single step\r\n";
				string expectedFail = testStudent.Name + " is hurt as " + spellTele.Name + " fails spectacularly!\r\n";
				if (sw.ToString() == expectedFail) {
					Assert.AreEqual(expectedFail, sw.ToString());
				} else {
					Assert.AreEqual(expectedPass, sw.ToString());
				}
				sw.Dispose();
			}
		}
	}
}

