﻿using System;
using System.Collections.Generic;

namespace OODesign
{
	public enum type {Healing, Transmogrification, Teleportation};
	public enum difficulty {Low, Medium, High};

	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.Write("Enter the name of the first student: ");
			Student lowSkilled = new Student(Console.ReadLine(), difficulty.Low, difficulty.Low, difficulty.Low);
			Console.WriteLine(lowSkilled.Name + " is the newest student at Swinwarts. They have little natural aptitude, with a low affinity for all schools of magic\n");
			Console.Write("Enter the name of the second student: ");
			Student mediumSkilled = new Student(Console.ReadLine(), difficulty.Medium, difficulty.Medium, difficulty.Medium);
			Console.WriteLine(mediumSkilled.Name + " is a second year student at Swinwarts. They've had some time to train, and have a moderate affinity for all schools of magic\n");

			SpellHealing cureLightWounds = new SpellHealing("Cure Light Wounds", difficulty.Low, "cures 1d8 of damage");
			SpellHealing cureMediumWounds = new SpellHealing("Cure Medium Wounds", difficulty.Medium, "cures 2d8 of damage");
			SpellHealing cureCriticalWounds = new SpellHealing("Cure Critical Wounds", difficulty.High, "cures 3d8 of damage");

			SpellTeleportation shortHop = new SpellTeleportation(lowSkilled.Name + "'s Quick Jump", difficulty.Low, "is teleported 20 feet away");
			SpellTeleportation mediumHop = new SpellTeleportation("Hagrid's Leap", difficulty.Medium, "is teleported hundreds of feet away");
			SpellTeleportation longHop = new SpellTeleportation("Greater Teleport", difficulty.High, "is teleported internationally");

			SpellTransmogrification lesserPolymorph = new SpellTransmogrification("Lesser Polymorph", difficulty.Low, "does the user's makeup");
			SpellTransmogrification mediumPolymorph = new SpellTransmogrification("Orc Imitation", difficulty.Medium, "turns into an orc");
			SpellTransmogrification greaterPolymorph = new SpellTransmogrification("Greater Polymorph", difficulty.High, "turns the into a frog");

			List<Spell> allSpells = new List<Spell>();
			allSpells.Add(cureLightWounds);
			allSpells.Add(cureMediumWounds);
			allSpells.Add(cureCriticalWounds);

			allSpells.Add(shortHop);
			allSpells.Add(mediumHop);
			allSpells.Add(longHop);

			allSpells.Add(lesserPolymorph);
			allSpells.Add(mediumPolymorph);
			allSpells.Add(greaterPolymorph);

			Random newRandom = new Random();

			foreach (Spell s in allSpells) {
				//25% chance of first year student having spell
				if (newRandom.Next(0, 100) < 25) {
					lowSkilled.Add(s);
				}
				//50% chance of second year student having spell
				if (newRandom.Next(0,100) < 50) {
					mediumSkilled.Add(s);
				}
			}

			//List spells in spellbook
			Console.WriteLine(lowSkilled.Name + "'s spellbook consists of:");
			foreach (Spell s in lowSkilled.Spellbook) {
				Console.WriteLine(s.Name);
			}
			Console.WriteLine("\nWhy don't you show us some of your spells, " + lowSkilled.Name + "?\n");

			//Select a spell at random from student's spellbook. Ask them to cast it.
			lowSkilled.Cast(lowSkilled.Spellbook[newRandom.Next(0, lowSkilled.Spellbook.Count)].Name);
			lowSkilled.Cast(lowSkilled.Spellbook[newRandom.Next(0, lowSkilled.Spellbook.Count)].Name);
			lowSkilled.Cast(lowSkilled.Spellbook[newRandom.Next(0, lowSkilled.Spellbook.Count)].Name);
			lowSkilled.Cast(lowSkilled.Spellbook[newRandom.Next(0, lowSkilled.Spellbook.Count)].Name);

			//List spells in spellbook
			Console.WriteLine("\n" + mediumSkilled.Name + " is a second year student here at Swinwarts. Their spellbook has:");
			foreach (Spell s in mediumSkilled.Spellbook) {
				Console.WriteLine(s.Name);
			}
			Console.WriteLine("\nPerhaps you could show us some of your work, " + mediumSkilled.Name + "?\n");

			//Select a spell at random from student's spellbook. Ask them to cast it.
			mediumSkilled.Cast(mediumSkilled.Spellbook[newRandom.Next(0, mediumSkilled.Spellbook.Count)].Name);
			mediumSkilled.Cast(mediumSkilled.Spellbook[newRandom.Next(0, mediumSkilled.Spellbook.Count)].Name);
			mediumSkilled.Cast(mediumSkilled.Spellbook[newRandom.Next(0, mediumSkilled.Spellbook.Count)].Name);
			mediumSkilled.Cast(mediumSkilled.Spellbook[newRandom.Next(0, mediumSkilled.Spellbook.Count)].Name);

			Console.ReadLine();
		}
	}
}

namespace OODesign
{
	public class Student
	{
		private string _name;
		private List<Tuple<type, difficulty>> _skill = new List<Tuple<type, difficulty>>();
		private List<Spell> _spellBook = new List<Spell>();
		private int _injured = new int();

		public string Name {
			get {
				return _name;
			}
		}

		public List<Spell> Spellbook {
			get {
				return _spellBook;
			}
		}

		public int Injured {
			get {
				return _injured;
			}
			set {
				_injured = value;
			}
		}

		public Student(string name, difficulty transDiff, difficulty healDiff, difficulty teleDiff)
		{
			_name = name;
			_skill.Add(new Tuple<type, difficulty>(type.Transmogrification, transDiff));
			_skill.Add(new Tuple<type, difficulty>(type.Healing, healDiff));
			_skill.Add(new Tuple<type, difficulty>(type.Teleportation, teleDiff));
		}

		public void Add(Spell addMe){
			//Adds spell to spellbook
			_spellBook.Add(addMe);
		}

		public void Cast(string name) {
			//Casts spell from given name.
			//Only matches first instance if multiple instances are in spellbook.
			foreach (Spell s in _spellBook) {
				if (s.Name == name) {
					s.Use(this);
					return;
				}
			}
		}

		public difficulty Skill(type type)
		{
			//Returns skill in given school
			foreach (Tuple<type, difficulty> t in _skill) {
				if (t.Item1 == type) {
					return t.Item2;
				}
			}
			return difficulty.Low;
		}
	}

	public abstract class Spell
	{
		protected string _name;
		protected type _type = new type();
		protected difficulty _difficulty = new difficulty();
		protected string _description;

		protected abstract void _fail(Student caster);
		protected Boolean _failCheck(Student caster)
		{
			double diffChance;

			switch (_difficulty) {
				case difficulty.Low:
					diffChance = 0.1;
					break;
				case difficulty.Medium:
					diffChance = 0.3;
					break;
				case difficulty.High:
					diffChance = 0.5;
					break;
				default:
					diffChance = 0;
					break;
			}

			double skillMulti;

			switch (caster.Skill(_type)) {
				case difficulty.Low:
					skillMulti = 1.5;
					break;
				case difficulty.Medium:
					skillMulti = 1.0;
					break;
				case difficulty.High:
					skillMulti = 0.5;
					break;
				default:
					skillMulti = 0;
					break;
			}

			double failChance = skillMulti * diffChance * 100;

			Random tempRandom = new Random();
			int tempIntRandom = tempRandom.Next(0, 100);

			if (failChance < tempIntRandom) {
				//Fails
				return false;
			} else {
				//Casts
				return true;
			}
		}

		public Spell()
		{
		}


		public virtual void Use(Student caster)
		{
			if (_failCheck(caster)) {
				Console.WriteLine(caster.Name + " " + _description);
			} else {
				_fail(caster);
			}
		}

		public string Name {
			get {
				return _name;
			}
		}
	}

	public class SpellTransmogrification : Spell
	{
		protected override void _fail(Student caster)
		{
			Console.WriteLine(caster.Name.ToString() + " is grown to enormous size as " + base.Name + " fails!");
		}

		public SpellTransmogrification(string name, difficulty diff, string description)
		{
			base._type = type.Transmogrification;
			base._name = name;
			base._difficulty = diff;
			base._description = description;
		}
	}

	public class SpellTeleportation : Spell
	{
		protected override void _fail(Student caster)
		{
			Console.WriteLine(caster.Name + " is hurt as " + base.Name + " fails spectacularly!");
			caster.Injured = caster.Injured + 1;
		}

		public SpellTeleportation(string name, difficulty diff, string description)
		{
			base._type = type.Teleportation;
			base._name = name;
			base._difficulty = diff;
			base._description = description;
		}
	}

	public class SpellHealing : Spell
	{
		protected override void _fail(Student caster)
		{
			Console.WriteLine(caster.Name + " is hurt as " + base.Name + " fails!");
			caster.Injured = caster.Injured + 1;
		}

		public SpellHealing(string name, difficulty diff, string description)
		{
			base._type = type.Teleportation;
			base._name = name;
			base._difficulty = diff;
			base._description = description;
		}

		public override void Use(Student caster)
		{
			if (_failCheck(caster)) {
				Console.WriteLine(caster.Name + " " + _description);
				caster.Injured = 0;
			} else {
				_fail(caster);
			}
		}
	}
}

