﻿using System;
/*
namespace OODesign
{
	public abstract class Spell
	{
		protected string _name = new string();
		protected type _type = new type();
		protected difficulty _difficulty = new difficulty();
		protected string _description = new string();

		private void _fail(Student caster);
		protected Boolean _failCheck(Student caster)
		{
			float diffChance;

			switch (_difficulty) {
				case difficulty.Low:
					diffChance = 0.1;
					break;
				case difficulty.Medium:
					diffChance = 0.3;
					break;
				case difficulty.High:
					diffChance = 0.5;
					break;
				default:
					diffChance = 0;
					break;
			}

			float skillMulti;

			switch (caster.Skill(_type)) {
				case difficulty.Low:
					skillMulti = 1.5;
					break;
				case difficulty.Medium:
					skillMulti = 1.0;
					break;
				case difficulty.High:
					skillMulti = 0.5;
					break;
				default:
					skillMulti = 0;
					break;
			}

			float failChance = skillMulti * diffChance * 100;

			Random tempRandom = new Random();
			int tempIntRandom = tempRandom.Next(0, 100);

			if (failChance < tempIntRandom) {
				//Fails
				return false;
			} else {
				//Casts
				return true;
			}
		}

		public Spell()
		{
		}


		public virtual void Use(Student caster)
		{
			if (_failCheck(caster)) {
				Console.WriteLine(caster.Name + " " + _description);
			} else {
				_fail(caster);
			}
		}

		public string Name {
			get {
				return _name;
			}
		}
	}

	public class SpellTransmogrification : Spell
	{
		private override void _fail(Student caster)
		{
			Console.WriteLine(caster.Name + " is grown to enormous size as " base.Name + " fails!");
		}

		public SpellTransmogrification(string name, difficulty diff, string description)
		{
			base._type = type.Transmogrification;
			base._name = name;
			base._difficulty = diff;
			base._description = description;
		}
	}

	public class SpellTeleportation : Spell
	{
		private override void _fail(Student caster)
		{
			Console.WriteLine(caster.Name + " is hurt as " base.Name + " fails spectacularly!");
			caster.Injured = caster.Injured + 1;
		}

		public SpellTeleportation(string name, difficulty diff, string description)
		{
			base._type = type.Teleportation;
			base._name = name;
			base._difficulty = diff;
			base._description = description;
		}
	}

	public class SpellHealing : Spell
	{
		private override void _fail(Student caster)
		{
			Console.WriteLine(caster.Name + " is hurt as " base.Name + " fails!");
			caster.Injured = caster.Injured + 1;
		}

		public SpellHealing(string name, difficulty diff, string description)
		{
			base._type = type.Teleportation;
			base._name = name;
			base._difficulty = diff;
			base._description = description;
		}

		public override void Use(Student caster)
		{
			if (_failCheck(caster)) {
				Console.WriteLine(caster.Name + " " + _description);
				caster.Injured = 0;
			} else {
				_fail(caster);
			}
		}
	}
}

*/