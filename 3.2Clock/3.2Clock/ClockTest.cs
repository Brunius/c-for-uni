﻿using NUnit.Framework;
using System;

namespace Clock
{
	[TestFixture()]
	public class ClockTest
	{
		[Test()]
		public void ClockTestInit()
		{
			Clock testObject;
			testObject = new Clock();

			//Test initialisation starts at zero
			Assert.AreEqual("0:00:00", testObject.Time);
		}

		[Test()]
		public void ClockTestTick()
		{
			Clock testObject;
			testObject = new Clock();

			//Test tick goes up by one
			testObject.tick();
			Assert.AreEqual("0:00:01", testObject.Time);
		}

		[Test()]
		public void ClockTestReset()
		{
			Clock testObject;
			testObject = new Clock();

			//Test reset resets
			testObject.tick();
			Assert.AreEqual("0:00:01", testObject.Time);
			testObject.reset();
			Assert.AreEqual("0:00:00", testObject.Time);
		}
	}
}

