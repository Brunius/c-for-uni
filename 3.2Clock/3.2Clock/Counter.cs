﻿using System;

namespace Clock
{
	public class Counter
	{
		private int _count;

		public int Value
		{
			get {
				return _count;
			}
		}

		private string _name;

		public string Name
		{
			get {
				return _name;
			}

			set {
				_name = value;
			}
		}

		public Counter (string name)
		{
			_name = name;
			_count = 0;
		}

		public void increment()
		{
			_count++;
		}

		public void reset()
		{
			_count = 0;
		}
	}
}

