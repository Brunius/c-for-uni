using NUnit.Framework;
using System;

namespace Clock
{
	[TestFixture()]
	public class CounterTest
	{
		[Test()]
		public void CounterTestBase()
		{
			Counter testObject;
			testObject = new Counter("test");

			//Test if the counter starts at zero
			Assert.AreEqual(0, testObject.Value);
		}

		[Test()]
		public void CounterTestIncrement()
		{
			Counter testObject;
			testObject = new Counter("test");

			//Test if incrementing the counter adds one to the count
			testObject.increment();
			Assert.AreEqual(1, testObject.Value);

			//Test if incrementing the counter multiple times matches up
			testObject.increment();
			testObject.increment();
			testObject.increment();
			testObject.increment();
			testObject.increment();
			Assert.AreEqual(6, testObject.Value);
		}

		[Test()]
		public void CounterTestReset()
		{
			Counter testObject;
			testObject = new Counter("test");

			//Test if resetting the counter actually resets it.
			testObject.increment();
			testObject.reset();
			Assert.AreEqual(0, testObject.Value);
		}
	}
}