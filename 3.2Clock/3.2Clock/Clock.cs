﻿using System;

namespace Clock
{
	public class Clock
	{
		private Counter _sec;
		private Counter _min;
		private Counter _hour;

		public Clock()
		{
			_sec = new Counter("seconds");
			_min = new Counter("minutes");
			_hour = new Counter("hours");
		}

		public void tick()
		{
			if (_sec.Value != 59) {
				_sec.increment();
			} else {
				_sec.reset();
				if (_min.Value != 59) {
					_min.increment();
				} else {
					_hour.increment();
					_min.reset();
				}
			}
		}

		public void reset()
		{
			_sec.reset();
			_min.reset();
			_hour.reset();
		}

		public string Time
		{
			get {
				string returnVal;
				returnVal = _hour.Value + ":";
				if (_min.Value < 10) {
					returnVal = returnVal + "0";
				}
				returnVal =  returnVal + _min.Value + ":";
				if (_sec.Value < 10) {
					returnVal = returnVal + "0";
				}
				returnVal = returnVal + _sec.Value;
				return returnVal;
			}
		}
	}
}

