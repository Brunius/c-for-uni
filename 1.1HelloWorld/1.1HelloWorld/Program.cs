﻿using System;

namespace HelloWorld
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Message myMessage;
			myMessage = new Message ("Hello World - from Message Object");
			myMessage.Print ();

			Message[] messages;
			messages = new Message[4];
			messages[0] = new Message ("Welcome back oh benevolent creator!");
			messages[1] = new Message ("Cool name, yo!");
			messages[2] = new Message ("Nice name!");
			messages[3] = new Message ("Your name is pretty silly");

			Console.Write("Enter name: ");
			string name = Console.ReadLine();

			if (name.ToLower () == "ben") {
				messages [0].Print ();
			} else if (name.ToLower () == "kyle") {
				messages [1].Print ();
			} else if (name.ToLower () == "michael") {
				messages [2].Print ();
			} else {
				messages [3].Print ();
			}

			Console.ReadLine ();
		}
	}
}
